require 'rspec'
require_relative '../model/contract'

describe Contract do
  let(:contract) do
    described_class.new(name: 'artear',
                        amount: 10_000,
                        content: ['Volver al Futuro'],
                        sign_date: Time.new('2019-01-01'),
                        frequency: 1,
                        repeats: 1,
                        signals: %w[volver canal13])
  end

  it 'initially a contract is not confirmed' do
    expect(contract.confirmed?).to eq false
  end

  context 'when not confirmed' do
    it 'a contract can be modified' do
      contract.update(amount: 200_000)
      expect(contract.get(:amount)).to eq 200_000
    end

    it 'a contract can not be amended' do
      expect { contract.amend(amount: 200_000) }.to raise_exception(ContractNotConfirmedError)
    end
  end

  context 'when created' do
    it 'there must be at least one content' do
      expect { described_class.new(amount: 100) }.to raise_exception(NoContentError)
    end
  end

  context 'when confirmed' do
    before(:each) do
      contract.confirm
    end

    it 'if confirmed, a contract can no longer be modifier' do
      expect { contract.update(amount: 200_000) }.to raise_exception(ContractAlreadyConfirmedError)
    end

    it 'a contract can be amended' do
      contract.amend(amount: 200_000)
      expect(contract.get(:amount)).to eq 200_000
    end
  end

  context 'when multiple amendments are applied' do
    before(:each) do
      contract.update(amount: 100, repeats: 1, frequency: 1)
      contract.confirm
      contract.amend(amount: 50)
      contract.amend(repeats: 7)
    end

    it 'current amount is 50' do
      expect(contract.get(:amount)).to eq 50
    end

    it 'original amount is 100' do
      expect(contract.get_original(:amount)).to eq 100
    end

    it 'current repeats is 7' do
      expect(contract.get(:repeats)).to eq 7
    end

    it 'repeats with first amendment is 1' do
      expect(contract.get_with_amendments(:repeats, 1)).to eq 1
    end

    it 'original repeats is 1' do
      expect(contract.get_original(:repeats)).to eq 1
    end

    it 'original frequency is 1' do
      expect(contract.get_original(:frequency)).to eq 1
    end

    it 'actual frequency is 1' do
      expect(contract.get(:frequency)).to eq 1
    end
  end
end
