require_relative 'exceptions/contract_already_confirmed_error'
require_relative 'exceptions/contract_not_confirmed_error'
require_relative 'contract_data'
require_relative 'amendment'

class Contract
  def initialize(contract_data = {})
    @contract_data = ContractData.new(contract_data)
    @amendments = []
    @confirmed = false
  end

  def confirm
    @confirmed = true
  end

  def confirmed?
    @confirmed
  end

  def update(data)
    validate
    @contract_data.update(data)
  end

  def get(key)
    apply_amendments_and_get(key, @amendments)
  end

  def get_original(key)
    @contract_data.get(key)
  end

  def get_with_amendments(key, amendments_count)
    apply_amendments_and_get(key, @amendments[0..amendments_count - 1])
  end

  def amend(amend_data)
    raise ContractNotConfirmedError unless confirmed?

    @amendments << Amendment.new(amend_data)
  end

  private

  def apply_amendments_and_get(key, amendments)
    data = @contract_data.clone
    amendments.each { |amendment| amendment.apply_to(data) }
    data.get(key)
  end

  def validate
    raise ContractAlreadyConfirmedError if confirmed?
  end
end
