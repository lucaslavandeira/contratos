require_relative 'exceptions/no_content_error'
class ContractData
  def initialize(data = {})
    validate_content(data[:content])
    @name = data[:name]
    @sign_date = data[:sign_date]
    @amount = data[:amount]
    @content = data[:content]
    @repeats = data[:repeats]
    @frequency = data[:frequency]
    @signals = data[:signals]
  end

  def update(data)
    data.each { |k, v| instance_variable_set("@#{k}", v) }
  end

  def get(key)
    instance_variable_get("@#{key}")
  end

  private

  def validate_content(content)
    raise NoContentError if content.nil?
    raise NoContentError if content.empty?
  end
end
