class Amendment
  def initialize(data)
    @data = data
  end

  def apply_to(contract_data)
    contract_data.update(@data)
  end
end
